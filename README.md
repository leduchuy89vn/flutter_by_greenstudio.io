# letsbuildthatapp

Project được thực hiển bởi Nhựt Huỳnh (nhut.huynh@greenstudio.io) demo việc phát triển một ứng dụng mobile app bằng Flutter đơn giản. Đây là toàn bộ mã nguồn của sản phẩm được tạo ra.
Ngoài ra, bạn có thể tìm thấy các tài liệu sau:

- Slide thuyết trình **FLUTTER - Building  beautiful  native apps in  record time** [tại đây](https://docs.google.com/presentation/d/1USxLQfKChzpReKkXtJ4keNsoOivdYyRtOaAv6OqW27s)
- Video hướng dẫn đọc mã nguồn, giải thích cấu trúc project và các mô tả khác [tại đây](https://www.youtube.com/watch?v=W-BUla1hUD0)
- Bạn có thể đọc thêm về [Biên Dịch và Thông Dịch](https://hackernoon.com/why-flutter-uses-dart-dd635a054ebf), tại sao Flutter lại là ngôn ngữ có khả năng thực thi nhanh hơn so với các framework Reactive khác

Green Studio, một startup trẻ chuyên thực hiện các dự án mobile app, khai phá & phân tích dữ liệu. Đội ngủ trẻ, năng động, yêu thích công nghệ và thích đối đầu với các thách thức to lớn. Green Studio chờ đón bạn gia nhập với chương trình tuyển sinh thực tập sinh mùa 2018 với 03 vị trí:

- 01 Lập trình viên Cross Platform trên nền Ract Native & Flutter
- 01 Lập trình viên backend trên nền Django & rest-django-framework
- 01 Thực tập sinh vị trí Mobile App Designer

Trong đó, chính sách thử việc:

- Thực tập hưởng lương theo năng lực, up-to 6.000.000đ/tháng
- Cấp máy Macbook Pro Ram 8G 256 SSD, hóa giá máy cho nhân viên sau 12 tháng làm việc

## Getting Started

Bạn có thể đọc thêm [tài liệu](https://flutter.io/) về Flutter trước khi bắt đầu.
