class CourseDetails {

  final String  name;
  final int     number; 
  final String  duration;
  final String  imageUrl;

  CourseDetails({this.name, this.number, this.duration, this.imageUrl});

  factory CourseDetails.fromJson(Map<String, dynamic> json) {
    return CourseDetails(

      number:   json["number"] as int,
      name:     json["name"] as String,
      imageUrl: json["imageUrl"] as String,
      duration: json["duration"] as String,
    );
  }
}