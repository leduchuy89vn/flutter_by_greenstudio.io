class Channel {

  final String name;
  final String profileImageUrl;
  final int numberOfSubscribers;   

  Channel({this.name, this.profileImageUrl, this.numberOfSubscribers});

  factory Channel.fromJSON(Map<String, dynamic> json) {
    return Channel(
      
      name                : json["name"] as String,
      profileImageUrl     : json["profileImageUrl"] as String,
      numberOfSubscribers : json["numberOfSubscribers"] as int
     );
  }
}