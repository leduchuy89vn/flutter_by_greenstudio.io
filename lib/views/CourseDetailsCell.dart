import 'package:flutter/material.dart';
import 'package:letsbuildthatapp/entities/Course.dart';
import 'package:letsbuildthatapp/entities/CourseDetails.dart';

class CourseDetailsCell extends StatelessWidget {
  @override

  final course;
  final courseDetails;

  CourseDetailsCell(this.course, this.courseDetails);
  
  Widget build(BuildContext context) {

    final courseItem        = Course.fromJson(this.course);
    final courseDetailsItem = CourseDetails.fromJson(this.courseDetails);

      return Container(
        padding: new EdgeInsets.all(15.0),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Image.network(courseDetailsItem.imageUrl, width: 150.0, height: 80.0, fit: BoxFit.cover,),
            new Expanded(
              child: new Container(
                padding: new EdgeInsets.only(left: 15.0, right: 15.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(courseDetailsItem.name, style: new TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),),
                    new Container(height: 5.0,),
                    new Text("Videos: " + courseDetailsItem.number.toString(), style: new TextStyle(fontSize: 13.0, fontWeight: FontWeight.normal, color: Colors.grey)),
                    new Container(height: 5.0,),
                    new Text("Duration: " + courseDetailsItem.duration, style: new TextStyle(fontSize: 13.0, fontWeight: FontWeight.normal, color: Colors.grey))
                  ],
                ),
              )
            )
          ],
        ),
      );
    }
 }

