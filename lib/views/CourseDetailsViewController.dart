import 'dart:convert';
import 'CourseDetailsCell.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:letsbuildthatapp/Entities/Course.dart';
import 'package:letsbuildthatapp/Entities/Channel.dart';

class CourseDetailsViewController extends StatefulWidget {
  @override

  final course;

  CourseDetailsViewController(this.course);

  State<StatefulWidget> createState() {
    return new CourseDetailsState(course);
  }
}

class CourseDetailsState extends State<CourseDetailsViewController> {
  var course;
  var courseDetails;

  CourseDetailsState(this.course);

  fetchCourseDetails() async {

    final url = "http://api.letsbuildthatapp.com/youtube/course_detail?id=" + this.course["id"].toString();
    final response = await http.get(url);

    if (response.statusCode == 200) {

      final courseDetailsJSON = json.decode(response.body);

      setState(() {
        this.courseDetails = courseDetailsJSON;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.fetchCourseDetails();
  }

  @override
  Widget build(BuildContext context) {
    
    final courseItem = Course.fromJson(this.course);
    final channelItem = Channel.fromJSON(this.course["channel"]);

    Widget titleSection = Container(
      padding: new EdgeInsets.all(15.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            padding: new EdgeInsets.only(bottom: 5.0),
            child: new Text(courseItem.name,
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
          ),
          new Text(courseItem.numberOfViews.toString() + " views",
                style: new TextStyle( fontSize: 15.0, color: Colors.grey, fontWeight: FontWeight.normal)
          )
        ],
      )
    );

    Column buildButtonColumn(IconData icon, String label) {
      Color color = Colors.grey;

      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Icon(icon, color: color,),
          new Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: new Text( label,
              style: new TextStyle( fontSize: 12.0, fontWeight: FontWeight.w400, color: color),
            ),
          )
        ],
      );
    }

    Widget buttonSection = Container(
      height: 50.0,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          buildButtonColumn(Icons.thumb_down, "Dislike"),
          buildButtonColumn(Icons.share, "Share"),
          buildButtonColumn(Icons.file_download, "Dowload"),
          buildButtonColumn(Icons.playlist_add, "Add to"),
        ],
      ),
    );

    Widget channelProfileSection = Container(
      child: new Container(
        margin: new EdgeInsets.only(top: 5.0, bottom: 5.0),
        height: 60.0,
        padding: new EdgeInsets.only(left: 15.0),
        child: new Row(
          
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              width: 50.0,
              height: 50.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: new NetworkImage(channelItem.profileImageUrl)
                  )
                ),
            ),
          new Container(
            padding: new EdgeInsets.all(10.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(channelItem.name,
                    style: new TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold)),
                new Container(height: 5.0),
                new Text(channelItem.numberOfSubscribers.toString() + " subcribers",
                    style: new TextStyle(fontSize: 13.0, fontWeight: FontWeight.normal, color: Colors.grey))
              ],
            ),
          ),
          new Container(
              padding: new EdgeInsets.only(left: 30.0),
              child: new Row(
                children: <Widget>[
                  new Icon(
                    Icons.subscriptions,
                    color: Colors.red,
                  ),
                  new Container(width: 10.0),
                  new Text("SUBSCRIBE",
                      style: new TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12.0))
                ],
              )
            )
          ],
        ),
      )
    );

    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.red,
        title: new Text("Videos"),
      ),
      body: new Center(
        child: new Column(
          children: <Widget>[
            new Image.network(courseItem.imageUrl, width: 600.0, height: 240.0, fit: BoxFit.cover),
            new Container(
              height: (MediaQuery.of(context).size.height - 350),
              child: new ListView.builder(
                itemCount: this.courseDetails == null ? 3 : this.courseDetails.length + 3,
                itemBuilder: (context, index) {
                  if (index == 0) {
                    
                    return titleSection;
                  } else if (index == 1) {

                    return buttonSection;
                  } else if (index == 2) {

                    return channelProfileSection;
                  } else if (this.courseDetails != null) {
                    final courseDetailsItem = this.courseDetails[index - 3];
                    
                    return new CourseDetailsCell(this.course, courseDetailsItem);
                  }
                },
              ),
            )
          ],
        )
      ),
    );
  }
}
