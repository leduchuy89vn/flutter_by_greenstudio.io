import 'dart:convert';
import './views/CoursesCell.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './views/CourseDetailsViewController.dart';

void main() => runApp(new PlaylistsViewController());

class PlaylistsViewController extends StatefulWidget {
  @override

  State<StatefulWidget> createState() {
    return new PlaylistsViewState();
  }
}

class PlaylistsViewState extends State<PlaylistsViewController> {
  
  var courses;
  var isLoadding = false; 

  fetchVideos() async {
    final url       = "http://api.letsbuildthatapp.com/youtube/home_feed";
    final response  = await http.get(url);

    if (response.statusCode == 200) {
      final map         = json.decode(response.body);
      final courseJSON  = map["videos"];

      setState(() {
        this.isLoadding = false;
        this.courses = courseJSON;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.fetchVideos();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.red,
          title: new Text("Let's Build That App"),
          actions: <Widget>[
            new IconButton(
              icon: new Icon(Icons.remove),
              onPressed: () {
                print("Reloadding...");
                setState(() {
                  this.isLoadding = true;
                });
                this.fetchVideos();
              },
            )
          ],
        ),
        body: new Center(
          child: this.isLoadding
              ? new CircularProgressIndicator()
              : new ListView.builder(
                  itemCount: this.courses != null ? this.courses.length : 0,
                  itemBuilder: (context, index) {
                    var courseItem = courses[index];

                    return new FlatButton(
                      padding: new EdgeInsets.all(0.0),
                      child: new CoursesCell(courseItem),
                      onPressed: () {
                        Navigator.push(context, new MaterialPageRoute(
                          builder: (context) => new CourseDetailsViewController(courseItem))
                        );
                      },
                    );
                  },
                ),
          ),
      ),
    );
  }
}
