import 'package:flutter/material.dart';
import 'package:letsbuildthatapp/Entities/Course.dart';

class CoursesCell extends StatelessWidget {

  final course;

  CoursesCell(this.course);

  @override
  Widget build(BuildContext context) {

    final courseItem = Course.fromJson(this.course);

     return Column(
       children: <Widget>[
         new Container(
          padding: new EdgeInsets.only(left: 15.0, right: 15.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                
              new Image.network(courseItem.imageUrl),
              new Container(height: 10.0,),
              new Text(courseItem.name, style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
              new Container(height: 5.0,),
              new Text(courseItem.numberOfViews.toString() + " views", style: new TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal, color: Colors.grey)),
            ],
          ),
         ),
         new Divider(color: Colors.grey)
       ],
     ); 
    }
}