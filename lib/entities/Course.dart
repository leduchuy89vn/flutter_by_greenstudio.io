import './Channel.dart';

class Course {

  final int     id;
  final String  name;
  final Channel channel;
  final String  imageUrl;
  final int     numberOfViews;

  Course({this.id, this.name, this.channel, this.imageUrl, this.numberOfViews});

  factory Course.fromJson(Map<String, dynamic> json) {
    return Course(

      id:             json["id"] as int,
      name:           json["name"] as String,
      imageUrl:       json["imageUrl"] as String,
      numberOfViews:  json["numberOfViews"] as int,
      channel:        Channel.fromJSON(json["channel"]) 
    );
  }
}